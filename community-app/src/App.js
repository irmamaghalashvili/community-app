import React from 'react';
import BigCommunitySection from './BigCommunitySection';
import JoinUsSection from './JoinUsSection';

function App() {
  return (
    <div>
      <BigCommunitySection />
      <JoinUsSection />
    </div>
  );
}

export default App;
