import React, { useState, useEffect } from 'react';

import './JoinUsSection';

const JoinUsSection = () => {
  const [email, setEmail] = useState('');
  const [isSubscribed, setIsSubscribed] = useState(localStorage.getItem("isSubscribed") === "true");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const storedEmail = localStorage.getItem("subscriptionEmail");
    if (storedEmail) {
      setEmail(storedEmail);
    }
  }, []);

  const handleEmailChange = (event) => {
    const emailValue = event.target.value;
    setEmail(emailValue);
    localStorage.setItem("subscriptionEmail", emailValue);
  };

  const handleSubscription = async () => {
    if (loading) return;

    try {
      setLoading(true);
      const response = await fetch('/subscribe', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });

      if (response.status === 422) {
        const data = await response.json();
        window.alert(data.error);
      } else if (response.ok) {
        setIsSubscribed(!isSubscribed);
      }

      setLoading(false);
    } catch (error) {
      console.error('Error subscribing:', error);
      setLoading(false);
    }
  };

  return (
    <section id="joinProgramSection" className="join-us">
      <h2 className="join-us-h2">
        {isSubscribed ? 'Join Our Advanced Program' : 'Join Our Program'}
      </h2>
      <p className="join-us-p">
        Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>
      <form onSubmit={(event) => event.preventDefault()}>
        <input
          type="email"
          placeholder="Email"
          value={email}
          onChange={handleEmailChange}
          className="join-us-form"
        />
        <button
          onClick={handleSubscription}
          className={`join-us-button ${loading ? 'loading' : ''}`}
          disabled={loading}
        >
          {loading ? 'Subscribing...' : (isSubscribed ? 'Unsubscribe' : 'Subscribe')}
        </button>
      </form>
    </section>
  );
};

export default JoinUsSection;
