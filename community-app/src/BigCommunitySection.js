import React, { useEffect, useState } from 'react';
import './BigCommunitySection.css';

const BigCommunitySection = () => {
  const [communityData, setCommunityData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('/community');
        if (!response.ok) {
          throw new Error('Network response was not ok.');
        }
        const data = await response.json();
        setCommunityData(data);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  const toggleVisibility = () => {
    setIsVisible(!isVisible);
  };

  return (
    <section id="community-section" className={`bigcommunitysection ${isVisible ? 'visible' : 'hidden'}`}>
      <h2 className="BigCommunitySection-h2">Big Community of People Like You</h2>
      <button onClick={toggleVisibility}>
        {isVisible ? 'Hide section' : 'Show section'}
      </button>
      {isVisible && (
        <div className="BigCommunitySection-div">
          {loading ? (
            <p>Loading community data...</p>
          ) : (
            <ul className="BigCommunitySection-ul">
              {communityData.map((person, index) => (
                <li key={index} className="community-li">
                  <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <img
                      className="community-avatar"
                      src={person.avatar}
                      alt={`${person.firstName} ${person.lastName} Avatar`}
                    />
                    <span style={{ fontWeight: 'bold' }}>
                      {person.firstName} {person.lastName}
                    </span>
                    <span>{person.position}</span>
                  </div>
                </li>
              ))}
            </ul>
          )}
        </div>
      )}
    </section>
  );
};


export default BigCommunitySection;
